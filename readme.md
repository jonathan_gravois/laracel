## Laravel-Excel Test

## Installation

* Clone repo to local environment
* From the webroot, run ```composer install```
* From the webroot, run ```touch .env```
* Edit /.env to connect to a database
* Open composer.json file and add this line to the *require* section
``` "maatwebsite/excel": "~2.0" ```
* Open ./config/app.php and add this line to the *providers* section ``` 'Maatwebsite\Excel\ExcelServiceProvider', ```
* Open ./config/app.php and add this line to the *aliasses* section
``` 'Excel' => 'Maatwebsite\Excel\Facades\Excel', ```
* Navigate to the main route and your excel will download